from django.db import models
from django.utils import timezone
from datetime import datetime


class Alarm(models.Model):
    alarm_id = models.CharField(max_length=40)
    severity = models.CharField(max_length=10)
    description = models.CharField(max_length=200)
    network_element_name = models.CharField(max_length=20)
    resource = models.CharField(max_length=20)
    layer = models.IntegerField()
    raised = models.DateTimeField(null=True, blank=True)
    cleared = models.DateTimeField(null=True, blank=True)
    situation = models.ForeignKey('Situation', null=True, on_delete=models.SET_NULL)
    removed = models.BooleanField(default=False)

    def get_color(self):
        severity_to_color = {'critical': 'danger', 'major': 'dark', 'minor': 'secondary', 'warning': 'warning',
                             'CR': 'danger', 'MJ': 'dark', 'MN': 'secondary', 'WN': 'warning'}
        return severity_to_color[self.severity]

    def get_uniqueness(self):
        alarms = Alarm.objects.filter(description=self.description).values()
        situation_set = set()
        for alarm in alarms:
            situation_set.add(alarm['situation_id'])
        return len(situation_set) / len(Situation.objects.all())

    def get_performance_change(self):
        situation = self.situation
        nodes = situation.get_affected_nodes()
        perf = {'EDTNLABPOC-01': {'AMP-1-2': 92, 'AMP-1-6': 62},'EDTNLABPOC-03':{'AMP-1-6': 81, 'AMP-1-2': 61}
                   ,'EDTNLABPOC-02': {'AMP-1-6': 50, 'AMP-1-2': 50},'FW9500-SITE1':{'1-6': 74, '1-1':30}
                   ,'FW9500-SITE3': {'1-6': 72, '1-1': 30}}
        if 'AMP-1-2' in self.resource or 'AMP-1-6' in self.resource:
            return 100-perf[self.network_element_name][self.resource[:7]]
        if 'AMP' in self.resource:
            return 60
        return 100


class Event(models.Model):
    status = models.CharField(max_length=20)
    notification_type = models.CharField(max_length=20)
    action = models.CharField(max_length=10)
    path = models.CharField(max_length=50) #lsp OR link
    raised = models.DateTimeField(null=True, blank=True)
    situation = models.ForeignKey('Situation', null=True, on_delete=models.SET_NULL)

    def get_color(self):
        status_to_color = {'Down': 'danger', 'Active': 'success', 'Up': 'success'}
        return status_to_color[self.status]


class Situation(models.Model):
    severity = models.CharField(max_length=10)
    root_cause_description = models.CharField(max_length=200)
    root_alarm = models.ForeignKey('Alarm', on_delete=models.CASCADE, related_name="root", blank=True, null=True)
    localized_failed_path = models.CharField(max_length=100)
    raised = models.DateTimeField(null=True, blank=True)
    cleared = models.DateTimeField(null=True, blank=True)
    failed_layer = models.IntegerField()

    def get_alarms(self, layer='none'):
        if layer != 'none':
            return Alarm.objects.filter(situation=self, layer=layer)
        return Alarm.objects.filter(situation=self)

    def get_events(self):
        return Event.objects.filter(situation=self)

    def get_services(self):
        return Service.objects.filter(situation=self)

    def get_affected_services(self):
        return Service.objects.filter(situation=self, up=False)

    def get_affected_nodes(self):
        services = self.get_affected_services()
        nodes = []
        for service in services:
            print('nodes nodes nodes nodes')
            print(service.path.split('#'))
            nodes += service.path.split('#')
        return list(set(nodes))


class Service(models.Model):
    situation = models.ForeignKey('Situation', on_delete=models.CASCADE)
    path = models.CharField(max_length=100)
    up = models.BooleanField()


class SituationToSituation(models.Model):
    similarity = models.IntegerField()
    situation_1 = models.ForeignKey('Situation', on_delete=models.CASCADE, related_name='situation_1')
    situation_2 = models.ForeignKey('Situation', on_delete=models.CASCADE, related_name='situation_2')


class Performance(models.Model):
    node = models.CharField(max_length=20)
    measurement_point = models.CharField(max_length=30)
    parameter = models.CharField(max_length=30)
    value = models.CharField(max_length=20)
    datetime = models.DateTimeField(null=True, blank=True)

