# Generated by Django 2.2.6 on 2019-10-28 02:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('models', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='alarm',
            name='layer',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='situationtoalarm',
            name='layer',
            field=models.IntegerField(default=-1),
        ),
        migrations.CreateModel(
            name='SituationToEvent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='models.Event')),
                ('situation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='models.Situation')),
            ],
        ),
    ]
