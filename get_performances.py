#!/usr/bin/env python

import json
import httplib2
import time
import datetime
import threading
import queue as Queue
import pandas as pd
import urllib
import requests
q = Queue.Queue()

mcp_baseRR = 'https://localhost:8081'
mcp_username = 'v0288002'
mcp_password = 'Telus1234'
virtuora_baseRR = 'https://localhost:8082/cxf'
orchestrator_baseRR='http://localhost:8084'

northstar_baseRR = 'https://localhost'
northstar_port = ':8083'


def get_mcp_token():
    data="username="+mcp_username+"&tenant=master&password="+mcp_password+"&timeout=60"
    ##httplib2 library is not thread-safe(causes SSL errors), so own instance per thread.
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    
    uriRR = '/'.join([mcp_baseRR, 'tron', 'api' , 'v1', 'tokens'])
    resp, content = h.request(
                              uri = uriRR,
                              method = 'POST',
                              headers={'Content-Type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'},
                              body=data
                              )
    if resp.status == 201: #Created. Used for successful POST requests.
        content_dict=json.loads(content)
        return content_dict['token']
    else:
        print(resp)
        return None


def get_mcp_fres_ids():
    token = get_mcp_token()
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uriRR = '/'.join([mcp_baseRR, 'nsi','api','search','fres?limit=200&serviceClass=Transport%20Client'])

    resp, content = h.request(
                              uri =uriRR,
                              method = 'GET',
                              headers={'Content-Type' : 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer '+token}
                              )
    print(resp)
    fres_json = json.loads(content.decode("utf-8"))
    return [fre['id'] for fre in fres_json['data']]


def get_mcp_tpe_location(id):
    token = get_mcp_token()
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uriRR = '/'.join([mcp_baseRR, 'nsi','api','tpes?id='+id])
    resp, content = h.request(
                              uri =uriRR,
                              method = 'GET',
                              headers={'Content-Type' : 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer '+token}
                              )
    print(resp)
    tpe_json = json.loads(content.decode("utf-8"))
    locations = tpe_json['data'][0]['attributes']['locations'][0]
    return locations['shelf']+'-'+locations['slot']+'-'+locations['port']

def get_mcp_fres():
    fres_ids = get_mcp_fres_ids()
    token = get_mcp_token()
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    fres_tpes = []
    for fre_id in fres_ids:
        uriRR = mcp_baseRR+'/revell/api/v1/serviceTrails/'+fre_id
        resp, content = h.request(
                          uri =uriRR,
                          method = 'GET',
                          headers={'Content-Type' : 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer '+token}
                          )
        print(resp)
        fre_details = json.loads(content.decode("utf-8"))
        network_constructs = {}
        for detail in fre_details['included'] :
            if detail['type'] == 'networkConstructs' :
                network_constructs[detail['id']] = detail['attributes']['displayData']['displayName']
        tpes = []
        tx_trail = []
        rx_trail = []
        prev_element = {'type':0,'id':0}
        
        for element in fre_details['data']['attributes']['trails'][0]['txDirection'] :
            if prev_element['type'] == 'networkConstruct' :
                tx_trail.append(network_constructs[prev_element['id']]+':'+get_mcp_tpe_location(element['id']))
            prev_element = element
        
        prev_element = {'type':0,'id':0}
        port_to_be_added = False
        rx_elements = fre_details['data']['attributes']['trails'][0]['rxDirection']
        rx_elements.reverse()
        for element in rx_elements :
            if not port_to_be_added :
                if element['type'] == 'tpes':
                    port_to_be_added = get_mcp_tpe_location(element['id'])
            if element['type'] == 'networkConstruct' :
                rx_trail.append(network_constructs[element['id']]+':'+port_to_be_added)
                port_to_be_added = False
            prev_element = element
        fres_tpes.append(tx_trail+[rx_trail[0]])
        fres_tpes.append(rx_trail+[tx_trail[0]])
#        for detail in fre_details['included'] :
#            if detail['type'] == 'tpes' and 'attributes' in detail and 'relationships' in detail:
#                locations = detail['attributes']['locations'][0]
#                nc_name = network_constructs[detail['relationships']['networkConstruct']['data']['id']]
#                tpes.append(nc_name+'-'+locations['shelf']+'-'+locations['slot']+'-'+locations['port'])
#        fres_tpes.append(list(set(tpes)))

    return fres_tpes
    

def get_virtuora_services():
    h = httplib2.Http(cache=None, disable_ssl_certificate_validation=True)
    
    uriRR = '/'.join([virtuora_baseRR, 'services-nb-otn','OTN','nwservices?_dc='+str(int(time.time()))])
    resp, content = h.request(uri = uriRR,
                              method = 'GET',
                              headers={'Accept' : 'application/json', 'Authorization':'Basic  YWRtaW46YWRtaW4='})
    print(resp)
    services_json = json.loads(content.decode("utf-8"))
    services = []
    for service in services_json['svcInfoList']['services'] :
        path = []
        for tp in service.get('routes')[0].get('routeTPseq') :
            path.append(tp['tid']+':'+tp['parentPTPAid'])
        services.append(path)
        services.append(path[::-1])
    return services



def get_boundary_links():
    return ['EDTNLABQFX-01:10.10.3.1&OTU/FW9500-SITE1:1-1-1', 'OTU/FW9500-SITE3:1-1-1&EDTNLABQFX-02:10.10.3.2', 'OTU/FW9500-SITE1:1-6-1&EDTNLABPOC-01:1-12-1', 'EDTNLABPOC-03:1-12-1&OTU/FW9500-SITE3:1-6-1', 'EDTNLABQFX-01:10.10.4.1&EDTNLABPOC-01:1-14-2', 'EDTNLABPOC-03:1-14-2&EDTNLABQFX-02:10.10.4.2']
    uriRR = '/'.join([orchestrator_baseRR, 'bpocore','market','api','v1' ,'resources?resourceTypeId=bpraboundaries.resourceTypes.Link'])
    h = httplib2.Http()
    resp, content = h.request(
                              uri = uriRR,
                              method = 'GET',
                              )
    print(resp)
    links_json = json.loads(content.decode("utf-8"))
    return [link['label'] for link in links_json['items']]


def northstar_get_uac_token():
    payload = {'grant_type': 'password','username': 'admin','password': 'telus55'}
    requests.packages.urllib3.disable_warnings()
    r = requests.post(northstar_baseRR + northstar_port+'/oauth2/token',data=payload,verify=False,auth=('admin', 'telus55'))
    data =r.json()
    if(('token_type' not in data) or ('access_token' not in data)):
        print ("Error: Invalid credentials")
        return None
    return data['access_token']


def get_northstar_lsps():
    return [['EDTNLABQFX-03:10.10.2.2','EDTNLABQFX-01:10.10.2.1','EDTNLABQFX-01:10.10.4.1',
             'EDTNLABQFX-02:10.10.4.2','EDTNLABQFX-02:10.10.0.1','EDTNLABQFX-04:10.10.0.2'],
            ['EDTNLABQFX-04:10.10.0.2','EDTNLABQFX-02:10.10.0.1','EDTNLABQFX-02:10.10.3.2',
             'EDTNLABQFX-01:10.10.3.1','EDTNLABQFX-01:10.10.2.1','EDTNLABQFX-03:10.10.2.2']
            ]
    token = northstar_get_uac_token()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uri = '/'.join([northstar_baseRR+ northstar_port, 'Northstar', 'API', 'v2', 'tenant', '1', 'topology', '1','te-lsps'])
    resp, content = h.request(
                              uri=uri,
                              method='GET',
                              headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
                              )
    lsps = json.loads(content.decode("utf-8"))
    #'calculatedEro'
    return lsps


def add_service_to_path(path, i, interface, services):
    if '/' in interface :
        interface = interface.split('/')[1]
    print(path)
    print(interface)
    for service in services :
        if service[0] == interface :
            return path[0:i+1] + service + path[i+1:]
    return path


def updated_path(path,boundary_links,services,is_virtuora) :
    for i in range(len(path)) :
        if not (is_virtuora and (i==0 or i == len(path)-1)) :
            for link in boundary_links :
                if path[i] in link :
                    link_splited = link.split('&')
                    if path[i] in link_splited[0] :
                        return add_service_to_path(path,i,link_splited[1],services)
                    return add_service_to_path(path,i,link_splited[0],services)


def calculate_paths():
    northstar_lsps = get_northstar_lsps()
    virtuora_services = get_virtuora_services()
    mcp_fres = get_mcp_fres()
    boundary_links = get_boundary_links()
    for i in range(len(virtuora_services)) :
        virtuora_services[i] = updated_path(virtuora_services[i],boundary_links,mcp_fres,1)
    
    for i in range(len(northstar_lsps)) :
        northstar_lsps[i] = updated_path(northstar_lsps[i],boundary_links,mcp_fres+virtuora_services,0)

    print('northstar : ')
    print(northstar_lsps)
    print('viruora : ')
    print(virtuora_services)
    print('mcp : ')
    print(mcp_fres)
    print('boundary links : ')
    print(boundary_links)


#for FRE performance historical infromation is not available , but for a node is available
#change it to /facilities -> historical will be available
#data = {
#    "data": {
#        "ncName": "string",
#        "reportingFacility": "string",
#        "granularity": "bin_24_hour",
#        "parameterNatives": [
#            "string"
#        ],
#        "range": {
#            "type": "relative",
#            "unit": "days",
#            "value": "string",
#            "startTime": "string",
#            "endTime": "string"
#        },
#        "synchronous": false
#    }
#}
def get_mcp_fre_performance(fre_id) :
    token = get_mcp_token()
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    data = {"data":{ "freIds": [ fre_id ], "granularity": "15-minute" , "synchronous": "true" }}
    uriRR = '/'.join([mcp_baseRR, 'tdc','api','v2','diag','operMeas'])
    resp, content = h.request(
                              uri = uriRR,
                              method = 'POST',
                              headers={'Content-Type' : 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer '+token},
                              body=json.dumps(data)
                              )
    print(resp)
    print(content)


def get_virtuora_performance():
    h = httplib2.Http(cache=None, disable_ssl_certificate_validation=True)
    
    uriRR = '/'.join([virtuora_baseRR, 'configmgmt-nb/configmgmt/collectCurrentPMData'])
    resp, content = h.request(uri = uriRR,
                              method = 'GET',
                              headers={'Accept' : 'application/json', 'Authorization':'Basic  YWRtaW46YWRtaW4='})
    print(resp)
    services_json = json.loads(content.decode("utf-8"))


def get_northstar_lsp_performance():
    token = northstar_get_uac_token()
    
    payload = {
        "startTime": {
            "$ref": "2019-06-21T00:00-00:00"
        },
        "endTime": {
            "$ref": "2019-06-22T00:00-00:00"
        },
        "interval": {
            "1d"
        },
        "aggregation": {
            "avg"
        }
    }

    data="startTime=2019-05-21T00:00-00:00&endTime=2019-06-22T00:00-00:00"
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uri = '/'.join([northstar_baseRR+ northstar_port, 'Northstar', 'API', 'v2', 'tenant', '1', 'statistics', 'te-lsps','fields'])
    resp, content = h.request(
                              body=data,
                              uri=uri,
                              method='POST',
                              headers={'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
                              )
    print(resp)
    print(content)
    lsps = json.loads(content.decode("utf-8"))
    print(lsps)
if __name__ == "__main__":
    #calculate_paths()
    fres_ids = get_mcp_fres_ids()
    #for fre_id in fres_ids :
        #fre_performance = get_mcp_fre_performance(fre_id)
    get_mcp_fre_performance(fres_ids[0])
#    get_northstar_lsp_performance()

