import sqlite3, csv, json
from datetime import datetime
from datetime import timedelta
from sqlite3 import Error
import os, sys
import re

statuses = [True, False]
services = [["QFX-3", "QFX-1", "POC-1", "POC-3", "QFX-2", "QFX-4"],
            ["QFX-3", "QFX-1", "FW-1", "POC-1", "POC-2", "POC-3", "FW-3", "QFX-2", "QFX-4"]]
failed_path = ["POC-1", "POC-2", "POC-3"]
table_columns = {'models_event': ['status', 'notification_type', 'action', 'path', 'raised'],
                 'models_alarm': ['alarm_id', 'severity', 'description', 'network_element_name', 'resource',
                                  'raised', 'cleared', 'layer', 'removed'],
                 'models_situation': ['severity', 'root_cause_description', 'root_alarm_id', 'localized_failed_path',
                                      'raised', 'cleared', 'failed_layer'],
                 'models_service': ['situation_id', 'path', 'up'],
                 'models_situationToSituation': ['similarity', 'situation_1_id', 'situation_2_id'],
                 'models_performance': ['node', 'measurement', 'value', 'datetime']
                 }


csv_columns = {'models_alarm0': ['id', 'Severity', 'Description', 'Device name', 'Resource', 'Raised', 'Cleared'],
               'models_alarm1': ['alarmIndex', 'severity', 'description', 'neId', 'aid',
                                 'emsDateTime(US/Mountain)', 'cleardateTime(US/Mountain)']}
default_root_alarm_id = 2118


def strip_nonalnum_re(word):
    return re.sub(r"^\W+|\W+$", "", word)


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def create_connection(db_file):
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn


def insert_row(conn, table, columns, row):
    columns = '(' + ','.join(columns) + ')'
    sql = 'INSERT INTO ' + table + columns + ' VALUES(' + '?,'*(len(row)-1) + '?)'
    print(sql)
    cur = conn.cursor()
    cur.execute(sql, row)
    conn.commit()
    return cur.lastrowid


def update_row(conn, table, columns, row, id):
    sql = 'UPDATE ' + table + ' SET '
    set_part = []
    for i in range(len(row)):
        set_part.append(columns[i] + " = " + str(row[i]))
    sql += ' , '.join(set_part)
    sql += ' WHERE id = ' + str(id)
    print(sql)
    cur = conn.cursor()
    cur.execute(sql)
    conn.commit()
    return cur.lastrowid


def select_alarms_events_by_date(conn, table, start_datetime, end_datetime):
    sql = 'SELECT * FROM ' + table + ' WHERE raised BETWEEN datetime(\'' \
          + str(start_datetime) + '\') AND datetime(\'' + str(end_datetime) + '\')'
    print(sql)
    cur = conn.cursor()
    cur.execute(sql)
    alarms = cur.fetchall()
    return alarms


def alarm_to_database(conn, csv_filename, database_name, layer):
    with open(csv_filename) as f:
        reader = csv.DictReader(f, delimiter=',')
        for row in reader:
            to_be_inserted = []
            if layer == '1':
                row['description'] = strip_nonalnum_re(row['description'])
                row['emsDateTime(US/Mountain)'] = datetime.strptime(row['emsDateTime(US/Mountain)'],
                                                                    "%Y-%m-%d %H:%M:%S MDT") + timedelta(hours=2)
                row['cleardateTime(US / Mountain)'] = datetime.strptime(row['cleardateTime(US / Mountain)'],
                                                                        "%Y-%m-%d %H:%M:%S MDT") + timedelta(hours=2)
            if layer == '0':
                row['Raised'] = datetime.strptime(row['Raised'], "%Y-%m-%dT%H:%M:%S-04:00")
            row['removed'] = False
            for col in csv_columns[database_name + layer]:
                to_be_inserted.append(row[col])
            to_be_inserted.append(layer)
            row_id = insert_row(conn, database_name, table_columns[database_name], to_be_inserted)


def event_to_database(conn, csv_filename, database_name, layer):
    with open(csv_filename) as f:
        time = True
        raised = ''
        for line in f.readlines():
            if len(line) < 10:
                continue
            if time:
                raised = datetime.strptime(line.strip(), "%Y-%m-%d %H:%M:%S.%f")
            else:
                data_json = json.loads(line.split(",data:")[1].replace("'", ''))
                if data_json['notificationType'] == 'lsp':
                    path = data_json['object']['from']['address']
                    for node in data_json['object']['liveProperties']['ero']:
                        path += '-' + node['address']
                elif data_json['notificationType'] == 'link':
                    path = data_json['object']['endA']['ipv4Address']['address'] + '-'
                    path += data_json['object']['endZ']['ipv4Address']['address']
                else:
                    time = not time
                    continue
                to_be_inserted = (data_json['object']['operationalStatus'], data_json['notificationType'],
                                  data_json['action'], path, raised)
                row_id = insert_row(conn, database_name, table_columns[database_name], to_be_inserted)
            time = not time


def create_situation(conn, start_datetime, end_datetime):
    # @todo need to change
    root_alarm_index = 0
    failed_layer = 0
    highest_severity = 'critical'
    situation_failed_path = '#'.join(failed_path)

    start_datetime = datetime.strptime(start_datetime, '%Y-%m-%d %H:%M:%S')
    end_datetime = datetime.strptime(end_datetime, '%Y-%m-%d %H:%M:%S')
    alarms = select_alarms_events_by_date(conn, 'models_alarm', start_datetime, end_datetime)
    events = select_alarms_events_by_date(conn, 'models_event', start_datetime, end_datetime)

    to_be_inserted = [highest_severity, alarms[root_alarm_index]['description'], alarms[root_alarm_index]['id'],
                      situation_failed_path, start_datetime, end_datetime, failed_layer]

    situation_id = insert_row(conn, 'models_situation', table_columns['models_situation'], to_be_inserted)
    # situation_id = 1
    print(situation_id)

    for alarm in alarms:
        update_row(conn, 'models_alarm', ['situation_id'], [situation_id], alarm['id'])
    for event in events:
        update_row(conn, 'models_event', ['situation_id'], [situation_id], event['id'])
    for service_index in range(len(services)):
        to_be_inserted = [situation_id, '#'.join(services[service_index]), statuses[service_index]]
        insert_row(conn, 'models_service', table_columns['models_service'], to_be_inserted)


def create_service_for_situation(conn, lsp, situation_id):
    to_be_inserted = [situation_id, '#'.join(lsp.fullpath), lsp.status]
    insert_row(conn, 'models_service', table_columns['models_service'], to_be_inserted)


def create_empty_situation(conn):
    to_be_inserted = ['critical', '', default_root_alarm_id, '', datetime.now(), '', 0]
    situation_id = insert_row(conn, 'models_situation', table_columns['models_situation'], to_be_inserted)
    return situation_id


def create_alarm_l0(conn, alarm, situation_id):
    if "clear-time" not in alarm:
        alarm["clear-time"] = None
    if 'PTS' in alarm["device-name"]:
        situation_id = None

    alarm["last-raise-time"] = datetime.strptime(alarm['last-raise-time'],"%Y-%m-%dT%H:%M:%S.%fZ") - timedelta(hours=5)
    to_be_inserted = (alarm["id"], alarm["condition-severity"].lower(), alarm["additional-text"],
                      alarm["device-name"], alarm["resource"], alarm["last-raise-time"],
                      alarm["clear-time"], 0, False, situation_id)
    row_id = insert_row(conn, 'models_alarm', table_columns['models_alarm']+['situation_id'], to_be_inserted)
    print(row_id)


def create_alarm_l1(conn, alarm, situation_id):
    severity_json = {'MJ': 'major', 'WN': 'warning', 'MN': 'minor', 'CR': 'critical'}

    if "cleardateTime" not in alarm:
        alarm["cleardateTime"] = None
    else:
        alarm["cleardateTime"] = datetime.utcfromtimestamp(int(alarm["cleardateTime"]) / 1000)
    to_be_inserted = (alarm["alarmIndex"], severity_json[alarm["severity"]], strip_nonalnum_re(alarm["description"]),
                      alarm["neId"], alarm["aId"], datetime.utcfromtimestamp(int(alarm["emsDateTime"]) / 1000),
                      alarm["cleardateTime"], 1, False, situation_id)
    row_id = insert_row(conn, 'models_alarm', table_columns['models_alarm']+['situation_id'], to_be_inserted)
    print(row_id)


def add_events_to_situation(conn, failure_time, situation_id):
    events = select_alarms_events_by_date(conn, 'models_event', failure_time, datetime.now())
    for event in events :
        update_row(conn, 'models_event', ['situation_id'], [situation_id], event['id'])
    print(events)

def similar_situations(conn, situation_1_id, situation_2_id):
    database_name = 'models_situationToSituation'
    to_be_inserted = [80, situation_1_id, situation_2_id]
    insert_row(conn, database_name, table_columns[database_name], to_be_inserted)


def rca(conn, situation_id, failed_path):
    pass


def performance_import(conn, start_time, end_time):
    pass


def write_to_db():
    database = r"db.sqlite3"
    conn = create_connection(database)
    conn.row_factory = dict_factory

    csv_filename = 'csv/alarms-l1-poc-1-1-2-6-2.csv'
    database_name = 'models_alarm'
    layer = '1'

    start_time = '2019-10-31 18:36:14'
    end_time = '2019-10-31 18:38:00'

    with conn:
        # alarm_to_database(conn, csv_filename, database_name, layer)
        # event_to_database(conn, csv_filename, database_name, layer)
        # create_situation(conn, start_time, end_time)
        similar_situations(conn, 3, 4)


if __name__ == '__main__':
    write_to_db()
