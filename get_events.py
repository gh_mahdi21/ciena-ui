from socketIO_client import SocketIO, BaseNamespace
import requests
from datetime import datetime
import json
from connect_to_db import *

northstar_baseRR = 'https://localhost'
northstar_port = ':8083'
northstar_port_number = '8083'

database = r"db.sqlite3"
conn = create_connection(database)
conn.row_factory = dict_factory

print(datetime.now())


class NSNotificationNamespace(BaseNamespace):
    def on_connect(self):
        print('Connected to %s:8443/restNotifications-v2' % northstar_baseRR)

    def on_event(key, name, data):
        data_json = data
        path = ''
        print(datetime.now())
        print("NorthStar Event: %r,data:%r" % (name, json.dumps(data)))
        try:
            if 'notificationType' in data_json:
                if data_json['notificationType'] == 'lsp':
                    path = data_json['object']['from']['address']
                    for node in data_json['object']['liveProperties']['ero']:
                        path += '-' + node['address']
                elif data_json['notificationType'] == 'link':
                    path = data_json['object']['endA']['ipv4Address']['address'] + '-'
                    path += data_json['object']['endZ']['ipv4Address']['address']

                to_be_inserted = (data_json['object']['operationalStatus'], data_json['notificationType'],
                                  data_json['action'], path, datetime.now())
                row_id = insert_row(conn, 'models_event', ['status', 'notification_type', 'action', 'path', 'raised'],
                                    to_be_inserted)
                conn.commit()
                print(row_id)
        except:
            print(data_json)




def northstar_subscribe():
    payload = {'grant_type': 'password', 'username': 'admin', 'password': 'telus55'}
    requests.packages.urllib3.disable_warnings()

    r = requests.post(northstar_baseRR + ':' + northstar_port_number + '/oauth2/token', data=payload, verify=False,
                      auth=('admin', 'telus55'))
    data = r.json()
    print('###3')
    print(data)
    if ('token_type' not in data) or ('access_token' not in data):
        print('Error: Invalid credentials')
        return

    headers = {'Authorization': "{token_type} {access_token}".format(**data)}
    socketIO = SocketIO(northstar_baseRR, northstar_port_number, verify=False, headers=headers)
    ns = socketIO.define(NSNotificationNamespace, '/restNotifications-v2')
    socketIO.wait()


if __name__ == '__main__':
    northstar_subscribe()
