(function ($) {
  'use strict'
  var graph_elements = [ // list of graph elements to start with
    {  data: { id: 'EDTNLABPOC-01'},renderedPosition:{x: 500, y:100}
    },{data: { id: 'EDTNLABPOC-02'},renderedPosition:{x: 600, y:170}
    },{data: { id: 'EDTNLABPOC-03'},renderedPosition:{x: 500, y:240}},

    {  data: { id: 'FW9500-SITE1'},renderedPosition:{x: 300, y:100}
    },{data: { id: 'FW9500-SITE2'},renderedPosition:{x: 400, y:100}
    },{data: { id: 'FW9500-SITE3'},renderedPosition:{x: 300, y:240}
    },{data: { id: 'FW9500-SITE4'},renderedPosition:{x: 400, y:240}},

    {  data: { id: 'EDTNLABQFX-01'},renderedPosition:{x: 200, y:100}
    },{data: { id: 'EDTNLABQFX-02'},renderedPosition:{x: 200, y:240}
    },{data: { id: 'EDTNLABQFX-03'},renderedPosition:{x: 100, y:100}
    },{data: { id: 'EDTNLABQFX-04'},renderedPosition:{x: 100, y:240}},

    {  data: { id: 'IXIA'},renderedPosition:{x: 100, y:170}
    },{data: { id: 'ONT-612'},renderedPosition:{x: 300, y:170}},


    {  data: { id: 'INV-1'},renderedPosition:{x: 300, y:70}
    },{data: { id: 'INV-2'},renderedPosition:{x: 501, y:70}
    },{data: { id: 'INV-3'},renderedPosition:{x: 300, y:270}
    },{data: { id: 'INV-4'},renderedPosition:{x: 501, y:270}
    },{data: { id: 'INV-5'},renderedPosition:{x: 200, y:50}
    },{data: { id: 'INV-6'},renderedPosition:{x: 500, y:50}
    },{data: { id: 'INV-7'},renderedPosition:{x: 200, y:290}
    },{data: { id: 'INV-8'},renderedPosition:{x: 500, y:290}},


    {  data: { id: 'EDTNLABPOC-01-EDTNLABPOC-02', source: 'EDTNLABPOC-01', target: 'EDTNLABPOC-02'}
    },{data: { id: 'EDTNLABPOC-01-EDTNLABPOC-03', source: 'EDTNLABPOC-01', target: 'EDTNLABPOC-03'}
    },{data: { id: 'EDTNLABPOC-02-EDTNLABPOC-03', source: 'EDTNLABPOC-02', target: 'EDTNLABPOC-03'}},


    {  data: { id: 'FW9500-SITE3-FW9500-SITE2', source: 'FW9500-SITE3', target: 'FW9500-SITE2'}
    },{data: { id: 'FW9500-SITE3-FW9500-SITE4', source: 'FW9500-SITE3', target: 'FW9500-SITE4'}
    },{data: { id: 'FW9500-SITE2-FW9500-SITE1', source: 'FW9500-SITE2', target: 'FW9500-SITE1'}
    },{data: { id: 'FW9500-SITE2-FW9500-SITE4', source: 'FW9500-SITE2', target: 'FW9500-SITE4'}
    },{data: { id: 'FW9500-SITE4-FW9500-SITE1', source: 'FW9500-SITE4', target: 'FW9500-SITE1'}
    },{data: { id: 'FW9500-SITE3-ONT-612', source: 'FW9500-SITE3', target: 'ONT-612'}
    },{data: { id: 'FW9500-SITE1-FW9500-SITE1', source: 'FW9500-SITE1', target: 'ONT-612'}},

    {  data: { id: 'EDTNLABQFX-01-EDTNLABQFX-03', source: 'EDTNLABQFX-01', target: 'EDTNLABQFX-03'}
    },{data: { id: 'EDTNLABQFX-02-EDTNLABQFX-04', source: 'EDTNLABQFX-02', target: 'EDTNLABQFX-04'}},

    {  data: { id: 'EDTNLABQFX-03-IXIA', source: 'EDTNLABQFX-03', target: 'IXIA'}
    },{data: { id: 'EDTNLABQFX-04-IXIA', source: 'EDTNLABQFX-04', target: 'IXIA'}},


    {  data: { id: 'EDTNLABQFX-01-FW9500-SITE1', source: 'EDTNLABQFX-01', target: 'FW9500-SITE1'}
    },{data: { id: 'EDTNLABQFX-02-FW9500-SITE3', source: 'EDTNLABQFX-02', target: 'FW9500-SITE3'}},


    {  data: { id: 'FW9500-SITE3-INV-1', source: 'FW9500-SITE3', target: 'INV-1'}
    },{data: { id: 'INV-1-INV-2', source: 'INV-1', target: 'INV-2'}
    },{data: { id: 'INV-2-EDTNLABPOC-01', source: 'INV-2', target: 'EDTNLABPOC-01'}
    },{data: { id: 'FW9500-SITE1-INV-3', source: 'FW9500-SITE1', target: 'INV-3'}
    },{data: { id: 'INV-3-INV-4', source: 'INV-3', target: 'INV-4'}
    },{data: { id: 'INV-4-EDTNLABPOC-03', source: 'INV-4', target: 'EDTNLABPOC-03'}
    },{data: { id: 'EDTNLABQFX-01-INV-5', source: 'EDTNLABQFX-01', target: 'INV-5'}
    },{data: { id: 'INV-5-INV-6', source: 'INV-5', target: 'INV-6'}
    },{data: { id: 'INV-6-EDTNLABPOC-01', source: 'INV-6', target: 'EDTNLABPOC-01'}
    },{data: { id: 'EDTNLABQFX-02-INV-7', source: 'EDTNLABQFX-02', target: 'INV-7'}
    },{data: { id: 'INV-7-INV-8', source: 'INV-7', target: 'INV-8'}
    },{data: { id: 'INV-8-EDTNLABPOC-03', source: 'INV-8', target: 'EDTNLABPOC-03'}}
  ];

  var graph_style = [ // the stylesheet for the graph
    {
      selector: 'node',
      style: {
        'background-color': '#666',
        'label': 'data(id)',
        'font-size': '8px',
        'width':'15px',
        'height':'15px',
      }
    },

    {
      selector: 'edge',
      style: {
        'width': 2,
        'line-color': '#ccc',
        'target-arrow-color': '#ccc',
      }
    },
    {
      selector: '.service',
      style: {
        'curve-style': 'bezier',
        'line-color': 'data(color)',
      }
    },
    {
      selector: '#INV-1, #INV-2, #INV-3, #INV-4, #INV-5, #INV-6, #INV-7, #INV-8',
      style: {
        'opacity': '0',
        'width': '1px',
        'height': '1px'
      }
    },
    {
      selector: '#IXIA, #ONT-612',
      style: {
        'shape': 'rectangle',
      }
    }
  ];

  var $cy = $('#cy');
  var $cy_loc = $('#cy_loc');
  var cy = cytoscape({
    container: $cy,
    elements: graph_elements,
    style: graph_style,
    layout: {
      name: 'preset'
    }
  });
  var cy_loc = cytoscape({
    container: $cy_loc,
    elements: graph_elements,
    style: graph_style,
    layout: {
      name: 'preset'
    }
  });

  var addEdge = function (cy, source, target, serviceNum, serviceStatus) {
    var upColor = ['teal', 'blue', 'aqua'];
    var downColor = ['red', 'crimson', 'darkred'];
    var color = serviceStatus ? upColor[serviceNum] : downColor[serviceNum];
    console.log(color);
    cy.add({
      data: { id: source.concat('-', target, '-', 'S', serviceNum), source: source, target: target, color: color},
      classes: ['S'.concat(serviceNum), 'service']
    })
  };

  var removeServices = function (cy) {
    cy.remove( cy.$('.S0', '.S1', '.S2') );
  };


  for (var i = 0; i < services.length; i++) {
    for (var j = 0; j < services[i].length; j++) {
      if (services[i][j] == 'EDTNLABQFX-01' && services[i][j + 1] == 'EDTNLABPOC-01') {
        services[i].splice(j + 1, 0, 'INV-5')
        services[i].splice(j + 2, 0, 'INV-6')
        localized_statuses[i].splice(j + 1, 0, localized_statuses[i][j])
        localized_statuses[i].splice(j + 2, 0, localized_statuses[i][j])
      }
      if (services[i][j] == 'FW9500-SITE3' && services[i][j + 1] == 'EDTNLABPOC-01') {
        services[i].splice(j + 1, 0, 'INV-1')
        services[i].splice(j + 2, 0, 'INV-2')
        localized_statuses[i].splice(j + 1, 0, localized_statuses[i][j])
        localized_statuses[i].splice(j + 2, 0, localized_statuses[i][j])
      }
      if (services[i][j] == 'EDTNLABPOC-03' && services[i][j + 1] == 'EDTNLABQFX-02') {
        services[i].splice(j + 1, 0, 'INV-8')
        services[i].splice(j + 2, 0, 'INV-7')
        localized_statuses[i].splice(j + 1, 0, localized_statuses[i][j])
        localized_statuses[i].splice(j + 2, 0, localized_statuses[i][j])
      }
      if (services[i][j] == 'EDTNLABPOC-03' && services[i][j + 1] == 'FW9500-SITE1') {
        services[i].splice(j + 1, 0, 'INV-4')
        services[i].splice(j + 2, 0, 'INV-3')
        localized_statuses[i].splice(j + 1, 0, localized_statuses[i][j])
        localized_statuses[i].splice(j + 2, 0, localized_statuses[i][j])
      }
    }
    if(['EDTNLABQFX-03', 'EDTNLABQFX-04'].includes(services[i][0])){
      services[i].splice(0, 0, 'IXIA')
      localized_statuses[i].splice(0, 0, localized_statuses[i][0])
    }

    if(['EDTNLABQFX-03', 'EDTNLABQFX-04'].includes(services[i][services[i].length-1])){
      services[i].splice(services[i].length, 0, 'IXIA')
      localized_statuses[i].splice(localized_statuses[i].length, 0, localized_statuses[i][localized_statuses[i].length-1])
    }

  }

  for (var i = 0; i < services.length; i++) {
    for (var j = 0; j < services[i].length-1; j++) {
      addEdge(cy, services[i][j], services[i][j+1], i, services_status[i])
    }
  }
  console.log(localized_statuses)
  console.log(services)
  for (var i = 0; i < services.length; i++) {
    for (var j = 0; j < services[i].length-1; j++) {
      addEdge(cy_loc, services[i][j], services[i][j+1], i, localized_statuses[i][j])
    }
  }
  var firstnode = cy.nodes().first();
  // var secondnode = cy.nodes()[1];
  var makeTippy = function(node, text){
    return tippy( node.popperRef(), {
      content: function(){
        var div = document.createElement('div');
        div.innerHTML = text;
        return div;
      },
      trigger: 'click',
      arrow: true,
      placement: 'bottom',
      hideOnClick: false,
      // multiple: true,
      sticky: true
    } );
  };
  var tippyA = makeTippy(firstnode, 'OSRP Line Operationally Blocked');
  var counter = 0
  firstnode.on('click', function(e){
    if(counter%2)
      tippyA.hide()
    else
      tippyA.show()
    counter+=1
  });

})(jQuery)
