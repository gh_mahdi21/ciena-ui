(function ($) {
  'use strict';
  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  };
  var mode      = 'index';
  var intersect = true;
  var $visitorsChart = $('#visitors-chart');
  console.log(timedates);

  var data = performances;
  var datasetLabels = lables;


  var colors = ['#007bff', '#ced4da', '#1a2e42', '#571a3f', '#476b49', '#b06454'];
  var datasets = [];
  for (var i=0; i<data.length; i++){
    datasets.push({
        type                : 'line',
        label               : datasetLabels[i],
        data                : data[i],
        backgroundColor     : 'transparent',
        borderColor         : colors[i],
        pointBorderColor    : colors[i],
        pointBackgroundColor: colors[i],
        fill                : false
        // pointHoverBackgroundColor: colors[i],
        // pointHoverBorderColor    : colors[i]
      })
  }
  var visitorsChart  = new Chart($visitorsChart, {
    data   : {
      labels  : timedates,
      datasets: datasets
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: true
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: 120
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })
})(jQuery);
