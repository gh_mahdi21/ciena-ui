(function ($) {
  'use strict';

  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  };
  var mode      = 'index';
  var intersect = true;
  var $visitorsChart = $('#visitors-chart');
  var labels = ['18th', '20th', '22nd', '24th', '26th', '28th', '30th'];
  console.log(timedates)
  var data = [[100, 120, 170, 167, 180, 177, 160], [60, 80, 70, 67, 80, 77, 100]];
  var datasetLabels = ['POC-3-AMP-1-6-2', 'POC-1-AMP-1-6-1'];
  var colors = ['#007bff', '#ced4da'];
  var datasets = [];
  for (var i=0; i<data.length; i++){
    datasets.push({
        type                : 'line',
        label               : datasetLabels[i],
        data                : data[i],
        backgroundColor     : 'transparent',
        borderColor         : colors[i],
        pointBorderColor    : colors[i],
        pointBackgroundColor: colors[i],
        fill                : false
        // pointHoverBackgroundColor: colors[i],
        // pointHoverBorderColor    : colors[i]
      })
  }
  var visitorsChart  = new Chart($visitorsChart, {
    data   : {
      labels  : timedates,
      datasets: datasets
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: true
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: 200
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })
})(jQuery);
