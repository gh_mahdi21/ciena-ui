(function ($) {
  'use strict'
  var graph_elements = [ // list of graph elements to start with
    {  data: { id: 'EDTNLABPOC-01'},renderedPosition:{x: 500, y:100}
    },{data: { id: 'EDTNLABPOC-02'},renderedPosition:{x: 600, y:170}
    },{data: { id: 'EDTNLABPOC-03'},renderedPosition:{x: 500, y:240}},

    {  data: { id: 'FW9500-SITE1'},renderedPosition:{x: 300, y:100}
    },{data: { id: 'FW9500-SITE2'},renderedPosition:{x: 400, y:100}
    },{data: { id: 'FW9500-SITE3'},renderedPosition:{x: 300, y:240}
    },{data: { id: 'FW9500-SITE4'},renderedPosition:{x: 400, y:240}},

    {  data: { id: 'EDTNLABQFX-01'},renderedPosition:{x: 200, y:100}
    },{data: { id: 'EDTNLABQFX-02'},renderedPosition:{x: 200, y:240}
    },{data: { id: 'EDTNLABQFX-03'},renderedPosition:{x: 100, y:100}
    },{data: { id: 'EDTNLABQFX-04'},renderedPosition:{x: 100, y:240}},

    {  data: { id: 'IXIA'},renderedPosition:{x: 100, y:170}
    },{data: { id: 'ONT-612'},renderedPosition:{x: 300, y:170}},


    {  data: { id: 'INV-1'},renderedPosition:{x: 300, y:70}
    },{data: { id: 'INV-2'},renderedPosition:{x: 501, y:70}
    },{data: { id: 'INV-3'},renderedPosition:{x: 300, y:270}
    },{data: { id: 'INV-4'},renderedPosition:{x: 501, y:270}
    },{data: { id: 'INV-5'},renderedPosition:{x: 200, y:50}
    },{data: { id: 'INV-6'},renderedPosition:{x: 500, y:50}
    },{data: { id: 'INV-7'},renderedPosition:{x: 200, y:290}
    },{data: { id: 'INV-8'},renderedPosition:{x: 500, y:290}},


    {  data: { id: 'EDTNLABPOC-01-EDTNLABPOC-02', source: 'EDTNLABPOC-01', target: 'EDTNLABPOC-02'}
    },{data: { id: 'EDTNLABPOC-01-EDTNLABPOC-03', source: 'EDTNLABPOC-01', target: 'EDTNLABPOC-03'}
    },{data: { id: 'EDTNLABPOC-02-EDTNLABPOC-03', source: 'EDTNLABPOC-02', target: 'EDTNLABPOC-03'}},


    {  data: { id: 'FW9500-SITE3-FW9500-SITE2', source: 'FW9500-SITE3', target: 'FW9500-SITE2'}
    },{data: { id: 'FW9500-SITE3-FW9500-SITE4', source: 'FW9500-SITE3', target: 'FW9500-SITE4'}
    },{data: { id: 'FW9500-SITE2-FW9500-SITE1', source: 'FW9500-SITE2', target: 'FW9500-SITE1'}
    },{data: { id: 'FW9500-SITE2-FW9500-SITE4', source: 'FW9500-SITE2', target: 'FW9500-SITE4'}
    },{data: { id: 'FW9500-SITE4-FW9500-SITE1', source: 'FW9500-SITE4', target: 'FW9500-SITE1'}
    },{data: { id: 'FW9500-SITE3-ONT-612', source: 'FW9500-SITE3', target: 'ONT-612'}
    },{data: { id: 'FW9500-SITE1-FW9500-SITE1', source: 'FW9500-SITE1', target: 'ONT-612'}},

    {  data: { id: 'EDTNLABQFX-01-EDTNLABQFX-03', source: 'EDTNLABQFX-01', target: 'EDTNLABQFX-03'}
    },{data: { id: 'EDTNLABQFX-02-EDTNLABQFX-04', source: 'EDTNLABQFX-02', target: 'EDTNLABQFX-04'}},

    {  data: { id: 'EDTNLABQFX-03-IXIA', source: 'EDTNLABQFX-03', target: 'IXIA'}
    },{data: { id: 'EDTNLABQFX-04-IXIA', source: 'EDTNLABQFX-04', target: 'IXIA'}},


    {  data: { id: 'EDTNLABQFX-01-FW9500-SITE1', source: 'EDTNLABQFX-01', target: 'FW9500-SITE1'}
    },{data: { id: 'EDTNLABQFX-02-FW9500-SITE3', source: 'EDTNLABQFX-02', target: 'FW9500-SITE3'}},


    {  data: { id: 'FW9500-SITE3-INV-1', source: 'FW9500-SITE3', target: 'INV-1'}
    },{data: { id: 'INV-1-INV-2', source: 'INV-1', target: 'INV-2'}
    },{data: { id: 'INV-2-EDTNLABPOC-01', source: 'INV-2', target: 'EDTNLABPOC-01'}
    },{data: { id: 'FW9500-SITE1-INV-3', source: 'FW9500-SITE1', target: 'INV-3'}
    },{data: { id: 'INV-3-INV-4', source: 'INV-3', target: 'INV-4'}
    },{data: { id: 'INV-4-EDTNLABPOC-03', source: 'INV-4', target: 'EDTNLABPOC-03'}
    },{data: { id: 'EDTNLABQFX-01-INV-5', source: 'EDTNLABQFX-01', target: 'INV-5'}
    },{data: { id: 'INV-5-INV-6', source: 'INV-5', target: 'INV-6'}
    },{data: { id: 'INV-6-EDTNLABPOC-01', source: 'INV-6', target: 'EDTNLABPOC-01'}
    },{data: { id: 'EDTNLABQFX-02-INV-7', source: 'EDTNLABQFX-02', target: 'INV-7'}
    },{data: { id: 'INV-7-INV-8', source: 'INV-7', target: 'INV-8'}
    },{data: { id: 'INV-8-EDTNLABPOC-03', source: 'INV-8', target: 'EDTNLABPOC-03'}}
  ];

  var graph_style = [ // the stylesheet for the graph
    {
      selector: 'node',
      style: {
        'background-color': '#666',
        'label': 'data(id)',
        'font-size': '8px',
        'width':'15px',
        'height':'15px',
      }
    },

    {
      selector: 'edge',
      style: {
        'width': 2,
        'line-color': '#ccc',
        'target-arrow-color': '#ccc',
      }
    },
    {
      selector: '.service',
      style: {
        'curve-style': 'bezier',
        'line-color': 'data(color)',
      }
    },
    {
      selector: '#INV-1, #INV-2, #INV-3, #INV-4, #INV-5, #INV-6, #INV-7, #INV-8',
      style: {
        'opacity': '0',
        'width': '1px',
        'height': '1px'
      }
    },
    {
      selector: '#IXIA, #ONT-612',
      style: {
        'shape': 'rectangle',
      }
    }
  ];

  var $cy = $('#cy');
  var cy = cytoscape({
    container: $cy,
    elements: graph_elements,
    style: graph_style,
    layout: {
      name: 'preset'
    }
  });

  var addEdge = function (cy, source, target, serviceNum, serviceStatus) {
    var upColor = ['teal', 'blue', 'aqua'];
    var downColor = ['red', 'crimson', 'darkred'];
    var color = serviceStatus ? upColor[serviceNum] : downColor[serviceNum];
    console.log(color);
    cy.add({
      data: { id: source.concat('-', target, '-', 'S', serviceNum), source: source, target: target, color: color},
      classes: ['S'.concat(serviceNum), 'service']
    })
  };

  var removeServices = function (cy) {
    cy.remove( cy.$('.S0', '.S1', '.S2') );
  };

  var cy_nodes = cy.elements('node')
  for(var i = 0; i < cy_nodes.length; i++){
    if (nodes.includes(cy_nodes[i].data('id'))){
      cy_nodes[i].style('background-color','red')
    }
  }

})(jQuery)
