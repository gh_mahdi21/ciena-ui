#!/usr/bin/env python

import json
import httplib2
import time
import datetime
import threading
import queue as Queue
import pandas as pd
import urllib
import requests

q = Queue.Queue()

mcp_baseRR = 'https://localhost:8081'
mcp_username = 'v0288002'
mcp_password = 'Telus1234'
# mcp_password = 'Telus!poc2017'
virtuora_baseRR = 'https://localhost:8082/cxf'
orchestrator_baseRR = 'http://localhost:8084'

northstar_baseRR = 'https://localhost'
northstar_port = ':8083'


class Service:
    def __init__(self, layer, status, path):
        self.layer = layer
        self.status = status
        self.dependencies = []

    def add_dependency(self, service):
        self.dependencies.append(service)


def get_mcp_token():
    data = "username=" + mcp_username + "&tenant=master&password=" + mcp_password + "&timeout=60"
    ##httplib2 library is not thread-safe(causes SSL errors), so own instance per thread.
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)

    uriRR = '/'.join([mcp_baseRR, 'tron', 'api', 'v1', 'tokens'])
    resp, content = h.request(
        uri=uriRR,
        method='POST',
        headers={'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'},
        body=data
    )
    if resp.status == 201:  # Created. Used for successful POST requests.
        content_dict = json.loads(content)
        return content_dict['token']
    else:
        return None


def get_mcp_fres_ids():
    token = get_mcp_token()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uriRR = '/'.join([mcp_baseRR, 'nsi', 'api', 'search', 'fres?limit=200&serviceClass=Transport%20Client'])

    #    uriRR = '/'.join([mcp_baseRR, 'mcpview/api/v1/tapi/core/context/connection/'])
    resp, content = h.request(
        uri=uriRR,
        method='GET',
        headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
    )
    fres_json = json.loads(content.decode("utf-8"))
    return [fre['id'] for fre in fres_json['data']]


def get_mcp_tpe_location(id):
    token = get_mcp_token()
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uriRR = '/'.join([mcp_baseRR, 'nsi', 'api', 'tpes?id=' + id])
    resp, content = h.request(
        uri=uriRR,
        method='GET',
        headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
    )
    tpe_json = json.loads(content.decode("utf-8"))
    locations = tpe_json['data'][0]['attributes']['locations'][0]
    return locations['shelf'] + '-' + locations['slot'] + '-' + locations['port']


def get_mcp_fres():
    fres_ids = get_mcp_fres_ids()
    token = get_mcp_token()
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    fres_tpes = []
    for fre_id in fres_ids:
        uriRR = mcp_baseRR + '/revell/api/v1/serviceTrails/' + fre_id
        resp, content = h.request(
            uri=uriRR,
            method='GET',
            headers={'Content-Type': 'application/json', 'Accept': 'application/json',
                     'Authorization': 'Bearer ' + token}
        )

        fre_details = json.loads(content.decode("utf-8"))
        network_constructs = {}

        for detail in fre_details['included']:
            if detail['type'] == 'networkConstructs':
                network_constructs[detail['id']] = detail['attributes']['displayData']['displayName']
        tpes = []
        tx_trail = []
        rx_trail = []
        prev_element = {'type': 0, 'id': 0}

        for element in fre_details['data']['attributes']['trails'][0]['txDirection']:
            if prev_element['type'] == 'networkConstruct':
                tx_trail.append(network_constructs[prev_element['id']] + ':' + get_mcp_tpe_location(element['id']))
            prev_element = element

        prev_element = {'type': 0, 'id': 0}
        port_to_be_added = False
        rx_elements = fre_details['data']['attributes']['trails'][0]['rxDirection']
        rx_elements.reverse()
        for element in rx_elements:
            if not port_to_be_added:
                if element['type'] == 'tpes':
                    port_to_be_added = get_mcp_tpe_location(element['id'])
            if element['type'] == 'networkConstruct':
                rx_trail.append(network_constructs[element['id']] + ':' + port_to_be_added)
                port_to_be_added = False
            prev_element = element
        fres_tpes.append(tx_trail + [rx_trail[0]])
        fres_tpes.append(rx_trail + [tx_trail[0]])
    #        for detail in fre_details['included'] :
    #            if detail['type'] == 'tpes' and 'attributes' in detail and 'relationships' in detail:
    #                locations = detail['attributes']['locations'][0]
    #                nc_name = network_constructs[detail['relationships']['networkConstruct']['data']['id']]
    #                tpes.append(nc_name+'-'+locations['shelf']+'-'+locations['slot']+'-'+locations['port'])
    #        fres_tpes.append(list(set(tpes)))

    return fres_tpes


def get_virtuora_services():
    h = httplib2.Http(cache=None, disable_ssl_certificate_validation=True)

    uriRR = '/'.join([virtuora_baseRR, 'services-nb-otn', 'OTN', 'nwservices?_dc=' + str(int(time.time()))])
    resp, content = h.request(uri=uriRR,
                              method='GET',
                              headers={'Accept': 'application/json', 'Authorization': 'Basic  YWRtaW46YWRtaW4='})
    services_json = json.loads(content.decode("utf-8"))
    services = []
    for service in services_json['svcInfoList']['services']:
        path = []
        for tp in service.get('routes')[0].get('routeTPseq'):
            path.append(tp['tid'] + ':' + tp['parentPTPAid'])
        services.append(path)
        services.append(path[::-1])
    return services


def get_boundary_links():
    return ['EDTNLABQFX-01:10.10.3.1&OTU/FW9500-SITE1:1-1-1', 'OTU/FW9500-SITE3:1-1-1&EDTNLABQFX-02:10.10.3.2',
            'OTU/FW9500-SITE1:1-6-1&EDTNLABPOC-01:1-13-2', 'EDTNLABPOC-03:1-13-2&OTU/FW9500-SITE3:1-6-1',
            'EDTNLABQFX-01:10.10.4.1&EDTNLABPOC-01:1-14-2', 'EDTNLABPOC-03:1-14-2&EDTNLABQFX-02:10.10.4.2']



def northstar_get_uac_token():
    payload = {'grant_type': 'password', 'username': 'admin', 'password': 'telus55'}
    requests.packages.urllib3.disable_warnings()
    r = requests.post(northstar_baseRR + northstar_port + '/oauth2/token', data=payload, verify=False,
                      auth=('admin', 'telus55'))
    data = r.json()
    if (('token_type' not in data) or ('access_token' not in data)):
        print ("Error: Invalid credentials")
        return None
    return data['access_token']


def get_northstar_lsps():
    token = northstar_get_uac_token()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uri = '/'.join(
        [northstar_baseRR + northstar_port, 'Northstar', 'API', 'v2', 'tenant', '1', 'topology', '1', 'te-lsps'])
    resp, content = h.request(
        uri=uri,
        method='GET',
        headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
    )
    lsps = json.loads(content.decode("utf-8"))
    lsps_paths = []
    lsps_status = []
    for lsp in lsps:
        # this line was due to handling failed LSP creation
        # if lsp['operationalStatus'] == 'Active' :
        path = [lsp['from']['address']]
        ero = lsp['liveProperties']['ero']
        for interface in ero:
            if 'address' in interface:
                ip = interface['address']
                # path.append(ip[:-1]+str(3-int(ip[-1:])))
                path.append(ip)
        path.append(lsp['to']['address'])
        lsps_paths.append(path)
        lsps_status.append(lsp['operationalStatus'] == 'Active')
    # 'calculatedEro'
    return [lsps_paths, lsps_status]


def add_service_to_path(path, i, interface, services):
    if '/' in interface:
        interface = interface.split('/')[1]
    for service in services:
        if service[0] == interface:
            return path[0:i] + service + path[i:]
    return path


def updated_path(path, boundary_links, services, is_virtuora):
    for i in range(len(path)):
        if not (is_virtuora and (i == 0 or i == len(path) - 1)):
            for link in boundary_links:
                if path[i] in link:
                    link_splited = link.split('&')
                    if path[i] in link_splited[0]:
                        return add_service_to_path(path, i, link_splited[1], services)
                    return add_service_to_path(path, i, link_splited[0], services)
    return path


def calculate_paths():
    lsp_res = get_northstar_lsps()
    northstar_lsps = lsp_res[0]
    northstar_lsps_status = lsp_res[1]
    virtuora_services = get_virtuora_services()
    virtuora_services_status = []  # todo
    mcp_fres = get_mcp_fres()
    mcp_fres_status = []  # todo
    boundary_links = get_boundary_links()

    for i in range(len(virtuora_services)):
        virtuora_services[i] = updated_path(virtuora_services[i], boundary_links, mcp_fres, 1)
    for i in range(len(northstar_lsps)):
        northstar_lsps[i] = updated_path(northstar_lsps[i], boundary_links, mcp_fres + virtuora_services, 0)

    print('northstar : ')
    print(northstar_lsps)
    print('viruora : ')
    print(virtuora_services)
    print('mcp : ')
    print(mcp_fres)
    print('boundary links : ')
    print(boundary_links)

    return [northstar_lsps, virtuora_services, mcp_fres], [northstar_lsps_status, virtuora_services_status,
                                                           mcp_fres_status]


def arr_contain_arr(arr1, arr2):
    pass


if __name__ == "__main__":
    paths, statuses = calculate_paths()
    # calculate_failed_poss(paths, statuses)


