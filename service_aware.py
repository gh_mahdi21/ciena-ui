#!/usr/bin/env python

import json
import httplib2
import time
from datetime import datetime
from datetime import timedelta
from threading import Thread
import queue as Queue
import urllib
import requests
from socketIO_client import SocketIO, BaseNamespace
import pickle
from connect_to_db import *
from urllib.parse import urlencode

q = Queue.Queue()

mcp_baseRR = 'https://localhost:8081'
mcp_username = 'v0288002'
mcp_password = 'Telus1234'
# mcp_password = 'Telus!poc2017'
virtuora_baseRR = 'https://localhost:8082/cxf'
orchestrator_baseRR = 'http://localhost:8084'
northstar_baseRR = 'https://localhost'
northstar_port = ':8083'
northstar_port_number = '8083'

database = r"db.sqlite3"
conn = create_connection(database)
conn.row_factory = dict_factory


class Service:
    def __init__(self, Serviceid, path, status, layer):
        self.layer = layer
        self.status = status
        self.path = path
        self.neighbor = ''
        self.dependencies = []
        self.fullpath = path
        self.dependencied = False

    def add_dependency(self, service, i):
        if not self.dependencied:
            self.dependencies.append(service)
            self.fullpath = self.fullpath[:i] + service.fullpath + self.fullpath[i:]
            print('full path')
            print(self.fullpath)
        self.dependencied = True


class NSNotificationNamespace(BaseNamespace):
    def on_connect(self):
        print('Connected to %s:8443/restNotifications-v2' % northstar_baseRR)

    def on_event(key, name, data):
        if name == 'lspEvent' and 'action' in data and data['action'] == "update" and 'object' in data and \
                data['object']['operationalStatus'] == 'Down':
            time.sleep(2)
            failure_time = datetime.now() - timedelta(seconds=5)
            failure_time_timestamp = time.time() - 10
            situation_id = create_empty_situation(conn)
            failed_path, failed_layer = get_failed_path(situation_id)
            sleep_time_for_alarms = failure_time + timedelta(seconds=75) - datetime.now()
            print('timedelta')
            print(sleep_time_for_alarms)
            if sleep_time_for_alarms > timedelta(seconds=0):
                time.sleep(sleep_time_for_alarms.total_seconds())
            l0_alarms = get_alarms_l0(failure_time, situation_id)
            l1_alarms = get_alarms_l1(failure_time_timestamp, situation_id)
            add_events_to_situation(conn, failure_time, situation_id)
            print(datetime.now())
            print("NorthStar Event: %r,data:%r" % (name, json.dumps(data)))
            exit()
        print(datetime.now())
        print("NorthStar Event: %r,data:%r" % (name, json.dumps(data)))


def get_alarms_l0(failure_time, situation_id):
    from_date = failure_time.strftime('%Y-%m-%dT%H%%3A%M%%3A%S-0500')
    to_date = datetime.now().strftime('%Y-%m-%dT%H%%3A%M%%3A%S-0500')
    data = "filter%5BlastRaisedTime%5D="+from_date+"%2C"+to_date+"&offset=0&pageSize=10000&sort%5B%5D=-last-raise-time"
    token = get_mcp_token()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uriRR = '/'.join([mcp_baseRR, 'nsa', 'api', 'v2_0', 'alarms', 'filter', 'filteredAlarms?'])
    uriRR += data
    print(uriRR)
    resp, content = h.request(
        uri=uriRR,
        method='GET',
        headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer '+token},
    )
    alarms = json.loads(content)['data']
    for alarm in alarms:
        alarm_attr = alarm["attributes"]
        if alarm_attr["condition-severity"] != "INFO":
            create_alarm_l0(conn, alarm_attr, situation_id)
    return alarms


def get_alarms_l1(raise_time, situation_id):
    h = httplib2.Http(cache=None, disable_ssl_certificate_validation=True)
    uriRR = '/'.join([virtuora_baseRR, 'fpm-fm', 'faultmgmt', 'fault-mgmt:alarm-summary-history'])
    content_dict = None
    resp, content = h.request(uri = uriRR,
                              method = 'POST',
                              headers={'Accept' : '*/*', 'Content-Type': 'application/json', 'Authorization':'Basic  YWRtaW46YWRtaW4='},
                              body='{"filterCriteria":{"aId":null,"aIdType":null,"ackDateTime":null,"ackUser":null,"alarmId":null,"clearDateTime":null,"clearUser":null,"conditionType":null,"description":null,"direction":null,"emsDateTime":null,"location":null,"neDateTime":null,"neId":null,"orderBy":null,"serviceAffect":null,"severity":null,"sortBy":null},"DayDetails":{"startDay":'+
                                   str(int(time.time()-86400))+'000,"endDay":'+str(int(time.time()))+'000}}'
                              )
    print('^^^^^^^^^^^^^^^')
    print('{"filterCriteria":{"aId":null,"aIdType":null,"ackDateTime":null,"ackUser":null,"alarmId":null,"clearDateTime":null,"clearUser":null,"conditionType":null,"description":null,"direction":null,"emsDateTime":null,"location":null,"neDateTime":null,"neId":null,"orderBy":null,"serviceAffect":null,"severity":null,"sortBy":null},"DayDetails":{"startDay":'+
                                   str(int(raise_time))+'000,"endDay":'+str(int(raise_time)+70)+'000}}')
    print(uriRR)
    if resp.status == 200: #OK
        content_dict = json.loads(content)['alarmRecords']
        for alarm in content_dict:
            if alarm['emsDateTime'] > int(raise_time) * 1000 and alarm['neId'] not in ['FW9500-SITE4', 'FW9500-SITE2']:
                create_alarm_l1(conn, alarm, situation_id)
    print("$$$$$$$$$$$$4")
    print(content_dict)
    uriRR = '/'.join([virtuora_baseRR, 'fpm-fm', 'faultmgmt', 'fault-mgmt:alarm-summary-active'])
    resp, content = h.request(uri = uriRR,
                              method = 'POST',
                              headers={'Accept' : '*/*', 'Content-Type': 'application/json', 'Authorization':'Basic  YWRtaW46YWRtaW4='},
                              body='{"filterCriteria":{"aId":null,"aIdType":null,"ackDateTime":null,"ackUser":null,"alarmId":null,"clearDateTime":null,"clearUser":null,"conditionType":null,"description":null,"direction":null,"emsDateTime":null,"location":null,"maintenanceTag":false,"maskTag":false,"neDateTime":null,"neId":null,"orderBy":null,"serviceAffect":null,"severity":null,"sortBy":null},"pageDetails":{"alarmPerPage":25,"startPage":0,"endPage":1}}'
                              )
    s = '{"filterCriteria":{"aId":null,"aIdType":null,"ackDateTime":null,"ackUser":null,"alarmId":null,"clearDateTime":null,"clearUser":null,"conditionType":null,"description":null,"direction":null,"emsDateTime":null,"location":null,"maintenanceTag":false,"maskTag":false,"neDateTime":null,"neId":null,"orderBy":null,"serviceAffect":null,"severity":null,"sortBy":null},"pageDetails":{"alarmPerPage":25,"startPage":0,"endPage":1}}'
    print(s)
    if resp.status == 200: #OK
        content_dict = json.loads(content)['alarmRecords']
        for alarm in content_dict:
            if alarm['emsDateTime'] > (int(raise_time)*1000) and alarm['neId'] not in ['FW9500-SITE4','FW9500-SITE2']:
                create_alarm_l1(conn, alarm, situation_id)
    print("$$$$$$$$$$$$")
    print(content_dict)
    return content_dict


def get_failed_path(situation_id=-1):
    failed_path = 'initial'
    failed_layer = 'not failed'
    all_services = calculate_dependencies()
    print(all_services)
    print_services(all_services)
    for lsp in all_services[2]:
        if not lsp.status:
            failed_layer = get_failed_layer(lsp)
            if situation_id != -1:
                update_row(conn, 'models_situation', ['failed_layer'], [failed_layer], situation_id)
            print('=============================')
            print(failed_layer)
            break

    if failed_layer != 'not failed':
        print(' failed layer : ')
        print(failed_layer)
        print(all_services[failed_layer])
        for service in all_services[failed_layer]:
            if not service.status:
                if failed_path == 'initial':
                    failed_path = service.path
                else:
                    for i in range(len(failed_path)):
                        print('&&&&&&&&&&&')
                        print(failed_path[i])
                        print(service.path)
                        print(service.neighbor.path)
                        if not (failed_path[i] in service.path or failed_path[i] in service.neighbor.path):
                            failed_path[i] = ''
                            print('path path ')
                            print(failed_path)
                    failed_path = list(filter(None, failed_path))
    if situation_id != -1:
        for lsp in all_services[2]:
            create_service_for_situation(conn, lsp, situation_id)
        update_row(conn, 'models_situation', ['localized_failed_path'], ['"'+'#'.join(failed_path)+'"'], situation_id)
    print('failed path : ')
    print(failed_path)
    return failed_path, failed_layer


def northstar_subscribe():
    payload = {'grant_type': 'password', 'username': 'admin', 'password': 'telus55'}
    requests.packages.urllib3.disable_warnings()

    r = requests.post(northstar_baseRR + ':' + northstar_port_number + '/oauth2/token', data=payload, verify=False,
                      auth=('admin', 'telus55'))
    data = r.json()
    print('###3')
    print(data)
    if ('token_type' not in data) or ('access_token' not in data):
        print('Error: Invalid credentials')
        return

    headers = {'Authorization': "{token_type} {access_token}".format(**data)}
    socketIO = SocketIO(northstar_baseRR, northstar_port_number, verify=False, headers=headers)
    ns = socketIO.define(NSNotificationNamespace, '/restNotifications-v2')
    socketIO.wait()


def get_mcp_token():
    data = "username=" + mcp_username + "&tenant=master&password=" + mcp_password + "&timeout=60"

    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)

    uriRR = '/'.join([mcp_baseRR, 'tron', 'api', 'v1', 'tokens'])
    resp, content = h.request(
        uri=uriRR,
        method='POST',
        headers={'Content-Type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'},
        body=data
    )
    if resp.status == 201:  # Created. Used for successful POST requests.
        content_dict = json.loads(content)
        return content_dict['token']
    else:
        print(resp)
        return None


def get_mcp_fres_ids(token):
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uriRR = '/'.join([mcp_baseRR, 'nsi', 'api', 'search', 'fres?limit=200&serviceClass=Transport%20Client'])

    #    uriRR = '/'.join([mcp_baseRR, 'mcpview/api/v1/tapi/core/context/connection/'])
    resp, content = h.request(
        uri=uriRR,
        method='GET',
        headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
    )
    fres_json = json.loads(content.decode("utf-8"))
    return [fre['id'] for fre in fres_json['data']]


def get_mcp_tpe_location(token, id):
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uriRR = '/'.join([mcp_baseRR, 'nsi', 'api', 'tpes?id=' + id])
    resp, content = h.request(
        uri=uriRR,
        method='GET',
        headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
    )
    tpe_json = json.loads(content.decode("utf-8"))
    locations = tpe_json['data'][0]['attributes']['locations'][0]
    return locations['shelf'] + '-' + locations['slot'] + '-' + locations['port']


def get_mcp_fres(return_path=False):
    token = get_mcp_token()
    fres_ids = get_mcp_fres_ids(token)
    h = httplib2.Http()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    fres = []
    service_objects = []
    for fre_id in fres_ids:
        uriRR = mcp_baseRR + '/revell/api/v1/serviceTrails/' + fre_id
        resp, content = h.request(
            uri=uriRR,
            method='GET',
            headers={'Content-Type': 'application/json', 'Accept': 'application/json',
                     'Authorization': 'Bearer ' + token}
        )

        fre_details = json.loads(content.decode("utf-8"))
        # print(fre_details)
        network_constructs = {}
        for detail in fre_details['included']:
            if detail['type'] == 'networkConstructs':
                network_constructs[detail['id']] = detail['attributes']['displayData']['displayName']
        tpes = []
        tx_trail = []
        rx_trail = []
        prev_element = {'type': 0, 'id': 0}

        for element in fre_details['data']['attributes']['trails'][0]['txDirection']:
            if prev_element['type'] == 'networkConstruct':
                tx_trail.append(network_constructs[prev_element['id']] + ':' + get_mcp_tpe_location(token, element['id']))
            prev_element = element

        prev_element = {'type': 0, 'id': 0}
        port_to_be_added = False
        rx_elements = fre_details['data']['attributes']['trails'][0]['rxDirection']
        rx_elements.reverse()
        for element in rx_elements:
            if not port_to_be_added:
                if element['type'] == 'tpes':
                    port_to_be_added = get_mcp_tpe_location(token, element['id'])
            if element['type'] == 'networkConstruct':
                rx_trail.append(network_constructs[element['id']] + ':' + port_to_be_added)
                port_to_be_added = False
            prev_element = element
        fres.append(tx_trail + [rx_trail[0]])
        fres.append(rx_trail + [tx_trail[0]])
        status = False
        if 'operationState' in fre_details['data']['attributes']:
            status = fre_details['data']['attributes']['operationState'] in ['In Service', 'Out of Service External']

        s1 = Service(fre_details['data']['id'], tx_trail + [rx_trail[0]], status, 0)
        s2 = Service(fre_details['data']['id'], rx_trail + [tx_trail[0]], status, 0)
        s1.neighbor = s2
        s2.neighbor = s1
        service_objects.append(s1)
        service_objects.append(s2)
    if return_path:
        return fres_tpes
    return service_objects


def get_virtuora_services(return_path=False):
    h = httplib2.Http(cache=None, disable_ssl_certificate_validation=True)

    uriRR = '/'.join([virtuora_baseRR, 'services-nb-otn', 'OTN', 'nwservices?_dc=' + str(int(time.time()))])
    resp, content = h.request(uri=uriRR,
                              method='GET',
                              headers={'Accept': 'application/json', 'Authorization': 'Basic  YWRtaW46YWRtaW4='})
    services_json = json.loads(content.decode("utf-8"))
    services = []
    service_objects = []
    for service in services_json['svcInfoList']['services']:
        path = []
        for tp in service.get('routes')[0].get('routeTPseq'):
            path.append(tp['tid'] + ':' + tp['parentPTPAid'])
        services.append(path)
        services.append(path[::-1])
        print('virtuora service')
        print(service)
        s1 = Service(service['svcID'], path, service['state'] == 'Active', 1)
        s2 = Service(service['svcID'], path[::-1], service['state'] == 'Active', 1)
        s1.neighbor = s2
        s2.neighbor = s1
        service_objects.append(s1)
        service_objects.append(s2)
    if return_path:
        return services
    return service_objects


def get_boundary_links():
    return ['EDTNLABQFX-01:10.10.3.1&FW9500-SITE1:1-1-1', 'FW9500-SITE3:1-1-1&EDTNLABQFX-02:10.10.3.2',
            'FW9500-SITE1:1-6-1&EDTNLABPOC-01:1-13-2', 'EDTNLABPOC-03:1-13-2&FW9500-SITE3:1-6-1',
            'EDTNLABQFX-01:10.10.4.1&EDTNLABPOC-01:1-14-2', 'EDTNLABPOC-03:1-14-2&EDTNLABQFX-02:10.10.4.2']
    uriRR = '/'.join([orchestrator_baseRR, 'bpocore', 'market', 'api', 'v1',
                      'resources?resourceTypeId=bpraboundaries.resourceTypes.Link'])
    h = httplib2.Http()
    resp, content = h.request(
        uri=uriRR,
        method='GET',
    )
    links_json = json.loads(content.decode("utf-8"))
    return [link['label'] for link in links_json['items']]


def northstar_get_uac_token():
    payload = {'grant_type': 'password', 'username': 'admin', 'password': 'telus55'}
    requests.packages.urllib3.disable_warnings()
    r = requests.post(northstar_baseRR + northstar_port + '/oauth2/token', data=payload, verify=False,
                      auth=('admin', 'telus55'))
    data = r.json()
    if (('token_type' not in data) or ('access_token' not in data)):
        print ("Error: Invalid credentials")
        return None
    return data['access_token']


def get_northstar_lsps():
    token = northstar_get_uac_token()
    h = httplib2.Http(".cache", disable_ssl_certificate_validation=True)
    uri = '/'.join(
        [northstar_baseRR + northstar_port, 'Northstar', 'API', 'v2', 'tenant', '1', 'topology', '1', 'te-lsps'])
    resp, content = h.request(
        uri=uri,
        method='GET',
        headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer ' + token}
    )
    lsps = json.loads(content.decode("utf-8"))
    lsps_paths = []
    lsps_status = []
    service_objects = []
    for lsp in lsps:
        # this line was due to handling failed LSP creation
        # if lsp['operationalStatus'] == 'Active' :
        # now there shouldn't be any failed LSP
        path = [lsp['from']['address']]
        ero = lsp['liveProperties']['ero']
        for interface in ero:
            if 'address' in interface:
                ip = interface['address']
                # path.append(ip[:-1]+str(3-int(ip[-1:])))
                path.append(ip)
        path.append(lsp['to']['address'])
        s1 = Service(lsp['lspIndex'], path, lsp['operationalStatus'] == 'Active', 2)
        s1.neighbor = s1
        service_objects.append(s1)
        lsps_paths.append(path)
        lsps_status.append(lsp['operationalStatus'] == 'Active')
    # 'calculatedEro'
    return service_objects, lsps_paths, lsps_status


def add_service_to_path(path, i, interface, services):
    for service in services:
        if service[0] == interface:
            return path[0:i] + service + path[i:]
    return path


def update_dependencie(service, boundary_links, lower_services):
    path = service.path
    if '10.10.4.2' in path :
        path.insert(path.index('10.10.4.2'), '10.10.4.1')
    if '10.10.3.2' in path :
        path.insert(path.index('10.10.3.2'), '10.10.3.1')

    ['EDTNLABQFX-01:10.10.3.1&FW9500-SITE1:1-1-1', 'FW9500-SITE3:1-1-1&EDTNLABQFX-02:10.10.3.2',
     'FW9500-SITE1:1-6-1&EDTNLABPOC-01:1-13-2', 'EDTNLABPOC-03:1-13-2&FW9500-SITE3:1-6-1',
     'EDTNLABQFX-01:10.10.4.1&EDTNLABPOC-01:1-14-2', 'EDTNLABPOC-03:1-14-2&EDTNLABQFX-02:10.10.4.2']
    for i in range(1,len(path)-1):
        for link in boundary_links:
            if path[i] in link:
                link_splited = link.split('&')
                if path[i] in link_splited[0]:
                    interface = link_splited[1]
                else:
                    interface = link_splited[0]
                for lower_service in lower_services:
                    if lower_service.path[0] == interface:
                        print('dependency')
                        print(service.path)
                        print(lower_service.path)
                        print(link_splited)
                        service.add_dependency(lower_service, i+1)
    # for i in range(1,len(path)-1):
    #     for link in boundary_links:
    #         if path[i] in link:
    #             link_splited = link.split('&')
    #             if path[i] in link_splited[0]:
    #                 interface = link_splited[1]
    #             else:
    #                 interface = link_splited[0]
    #             for lower_service in lower_services:
    #                 if (lower_service.path[0] == interface) and (lower_service.path[-1]):
    #                     print('dependency')
    #                     print(service.path)
    #                     print(lower_service.path)
    #                     print(link_splited)
    #                     service.add_dependency(lower_service, i)


    return service


def calculate_dependencies():
    virtuora_services = get_virtuora_services()
    print('virtura done')
    northstar_lsps, lsps_path, lsps_status = get_northstar_lsps()
    print('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
    print(lsps_path)
    print('LSP done')
    mcp_fres = get_mcp_fres()
    print('FRE done')
    boundary_links = get_boundary_links()

    for i in range(len(virtuora_services)):
        virtuora_services[i] = update_dependencie(virtuora_services[i], boundary_links, mcp_fres)
    for i in range(len(northstar_lsps)):
        northstar_lsps[i] = update_dependencie(northstar_lsps[i], boundary_links, mcp_fres + virtuora_services)

    print('northstar : ')
    print(northstar_lsps)
    print('viruora : ')
    print(virtuora_services)
    print('mcp : ')
    print(mcp_fres)
    print('boundary links : ')
    print(boundary_links)
    return [mcp_fres, virtuora_services, northstar_lsps]


def print_services(all_services):
    print('printing : ')
    for i in range(3):
        for j in range(len(all_services[i])):
            print(all_services[i][j])
            print(all_services[i][j].path)
            print(all_services[i][j].status)


def get_failed_layer(service):
    print('dependecies : ')
    print(service.dependencies)
    layer = 100
    for serv in service.dependencies:
        print('serv : ')
        print(serv)
        l = get_failed_layer(serv)
        layer = min(layer, l)
    if not service.status:
        return min(layer, service.layer)
    return min(layer, 100)


if __name__ == "__main__":
    # l0, l1, l2 = calculate_dependencies()
    # all_services = [l0,l1,l2]
    # print_services(all_services)

    northstar_subscribe()
    # get_alarms_l0(datetime.now(), 17)

    # get_alarms_l1(1573180321, 17)
    # add_events_to_situation(conn, datetime(2019, 11, 8, 0, 0, 0), 16)
    # get_alarms_l0(datetime(2019, 11, 8, 5, 50, 0), 16)
    # print('START subscription')
    # northstar_notifications_collector = Thread(target=northstar_subscribe())
    # print('northstar_collector started')
    # northstar_notifications_collector.start()
    # northstar_notifications_collector.join()
    # print('END')

