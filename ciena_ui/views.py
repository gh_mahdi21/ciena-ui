from django.shortcuts import render
import os, csv, json, re
from django.conf import settings
from itertools import groupby
from models.models import *
from datetime import datetime, timedelta
from itertools import chain
from django.db.models import Q
from django.db.models import Count
from service_aware import calculate_dependencies, get_failed_layer
import random

severity_map = {'warning': ['warning', 'WN'], 'minor': ['minor', 'MN'], 'major': ['major', 'MJ'],
                'critical': ['critical', 'CR', 'Down'], 'success': ['Up', 'Active']}

static_services = [["EDTNLABQFX-03", "EDTNLABQFX-01", "EDTNLABPOC-01", "EDTNLABPOC-03", "EDTNLABQFX-02", "EDTNLABQFX-04"],
            ["EDTNLABQFX-03", "EDTNLABQFX-01", "FW9500-SITE1", "EDTNLABPOC-01", "EDTNLABPOC-02", "EDTNLABPOC-03", "FW9500-SITE3", "EDTNLABQFX-02", "EDTNLABQFX-04"]]

map_ip = {'172.16.0.1': '01', '172.16.0.2': '02', '172.16.0.3': '03', '172.16.0.4': '04',
              '10.10.2.2': '03', '10.10.2.1': '01', '10.10.4.1': '01', '10.10.3.1': '01', '10.10.4.2': '02',
              '10.10.3.2': '02','10.10.0.1': '02', '10.10.0.2': '04'}



def strip_nonalnum_re(word):
    return re.sub(r"^\W+|\W+$", "", word)


def get_alarm_color(severity):
    severity_to_color = {'critical': 'danger', 'major': 'dark', 'minor': 'secondary', 'warning': 'warning',
                         'CR': 'danger', 'MJ': 'dark', 'MN': 'secondary', 'WN': 'warning'}
    return severity_to_color[severity]


def get_alarm_severity(severity):
    severity_to_severity = {'CR': 'critical', 'MJ': 'major', 'MN': 'minor', 'WN': 'warning'}
    return severity_to_severity[severity]


def parse_string(str):
    str = str.replace("\\", "")
    return str.replace("\/", "")


#todo need to change node name if it is used
def parse_services(paths):
    if isinstance(paths[0], list):
        ret = []
        for path in paths:
            ret.append(parse_path(path))
        return ret
    else:
        return parse_path(paths)


def get_localized_statuses(services, failed_path):
    services_localized_statuses = []
    prefix = failed_path[0][:10]
    prefix_to_prefix = {'EDTNLABQFX': 'FW9500-SIT', 'FW9500-SIT': 'EDTNLABPOC', 'EDTNLABPOC': 'qweqweqweqw'}
    for service in services:
        service_localized_statuses = []
        for i in range(len(service)-1):
            service_up = 1
            for j in range(len(failed_path)-1):
                if failed_path[j] == service[i] and failed_path[j+1] == service[i+1]\
                        or(service[i] in failed_path and prefix_to_prefix[prefix] in service[i+1])\
                        or(service[i+1] in failed_path and prefix_to_prefix[prefix] in service[i]):
                    service_up = 0
            service_localized_statuses.append(service_up)
        services_localized_statuses.append(service_localized_statuses)
    return services_localized_statuses


def get_event(severity, t, situation_id):
    if situation_id and severity:
        return Event.objects.filter(situation=situation_id, status__in=severity_map[severity]).order_by('-raised')
    if situation_id:
        return Event.objects.filter(situation=situation_id).order_by('-raised')
    return Event.objects.filter(raised__gte=(datetime.now() - timedelta(minutes=int(t)))).order_by('-raised')


def get_alarms(layer, severity, t=0, situation_id=0):
    if situation_id and severity:
        return Alarm.objects.filter(layer=str(layer), severity__in=severity_map[severity], situation=situation_id).order_by('-raised')
    if situation_id:
        return Alarm.objects.filter(layer=str(layer), situation=situation_id).order_by('-raised')
    return Alarm.objects.filter(layer=str(layer), raised__gte=(datetime.now() - timedelta(minutes=int(t)))).order_by('-raised')


def index(request):
    return render(request, 'index.html')


def topology(request):
    paths = []
    statuses = []

    failed_path = 'initial'
    failed_layer = 'not failed'
    all_services = calculate_dependencies()
    print("all services")
    print(all_services)

    failed_layer_list=[]
    for lsp in all_services[2]:
        if not lsp.status:
            failed_layer = get_failed_layer(lsp)
    if len(failed_layer_list):
        failed_layer = min(failed_layer_list)
    if failed_layer != 'not failed':
        for service in all_services[failed_layer]:
            if not service.status:
                if failed_path == 'initial':
                    failed_path = service.path
                else:
                    for i in range(len(failed_path)):
                        if not (failed_path[i] in service.path or failed_path[i] in service.neighbor.path):
                            failed_path[i] = ''
                    failed_path = list(filter(None, failed_path))

    for lsp in all_services[2]:
        paths.append(lsp.fullpath)
        statuses.append(lsp.status)


    print(paths)
    print(statuses)
    static_statuses = statuses #lsp status
    print(failed_path)
    static_services = parse_services(paths) #passing lsp full path only

    if failed_path != 'initial':
        localized_statuses = get_localized_statuses(static_services, parse_services(failed_path))
    else:
        localized_statuses = []
        for s in paths:
            localized_statuses.append([1 for x in range(len(s))])

    print('++++++++++++++++++++++++++++')
    print(localized_statuses)
    print(static_services)

    return render(request, 'topology.html', {'services': json.dumps(static_services), 'services_status':
        json.dumps(static_statuses),'localized_statuses': localized_statuses})


def alarms(request):
    t = request.GET.get('t', 0)
    situation_id = request.GET.get('situation', False)
    severity = request.GET.get('severity', False)
    alarms_l0 = get_alarms(0, severity, t, situation_id)
    alarms_l1 = get_alarms(1, severity, t, situation_id)
    alarms_l2 = get_event(severity, t, situation_id)
    result_list = list(chain(alarms_l0, alarms_l1, alarms_l2))
    result_list.sort(key=lambda x: x.raised, reverse=True)
    return render(request, 'alarms.html', {'t': t, 'alarms_l0': alarms_l0, 'alarms_all': result_list,
                                           'alarms_l2': alarms_l2, 'alarms_l1': alarms_l1, 'situation_id':
                                           situation_id})


def situations(request):
    situation_id = request.GET.get('situation', False)
    situations = Situation.objects.all()
    if situation_id:
        situations = Situation.objects.filter(id=situation_id)
    situations_data = []
    for situation in situations:
        if situation.root_alarm.description == "Not Assigned Yet":
            alarms = Alarm.objects.filter(situation=situation,layer=situation.failed_layer)
            for alarm in alarms:
                if "Software Reset" in alarm.description or "Input Loss Of Signal" in alarm.description:
                    situation.root_alarm = alarm
                    situation.save()
                    break
                if "OSRP" in alarm.description:
                    situation.root_alarm = alarm
                    situation.save()
            alarms = Alarm.objects.filter(situation=situation, description__in=["Input Loss Of Signal",
                     "OSRP Line Operationally Blocked"], network_element_name="EDTNLABPOC-01",
                                          layer=situation.failed_layer)
            for alarm in alarms:
                if alarm.description == "OSRP Line Operationally Blocked":
                    situation.root_alarm = alarm
                    situation.save()
            for alarm in alarms:
                if alarm.description == "Input Loss Of Signal":
                    situation.root_alarm = alarm
                    situation.save()
    for situation in situations:
        s2s = SituationToSituation.objects.filter(Q(situation_1=situation)|Q(situation_2=situation))
        if not len(s2s) and len(situations)>1:
            csimilarity(situation, situations)
    for situation in situations:
        alarm_count = len(Alarm.objects.filter(situation=situation))
        alarm_count += len(Event.objects.filter(situation=situation))
        situation_to_situation = SituationToSituation.objects.filter(Q(similarity__gte=20),Q(situation_1=situation)|Q(situation_2=situation))\
            .order_by('-similarity')[:3]
        print('nodes nodes nodes')
        print(situation.get_affected_nodes())
        situations_data.append({'id': situation.id, 'severity': situation.severity, 'color': 'danger',
                                'probable_root_alarm': parse_string(situation.root_alarm.description),
                                'probable_root_alarm_id': situation.root_alarm.id, 'alarm_count': alarm_count,
                                'affected_nodes': list(set(parse_path(situation.get_affected_nodes()))),
                                'failed_layer': situation.failed_layer,'affected_services':
                                    situation.get_affected_services(), 'situation_to_situation': situation_to_situation,
                                'raised': situation.raised, 'cleared': situation.cleared})

    return render(request, 'situations.html', {'situations': situations_data})


def parse_path(nodes):
    ret_nodes = []
    for node in nodes:
        if 'EDTNLABPOC' in node or 'FW9500-SITE' in node:
            ret_nodes.append(node.split(":")[0])
        elif node in map_ip:
            ret_nodes.append("EDTNLABQFX-" + map_ip[node])
    ret_nodes = [k for k, g in groupby(ret_nodes)]
    return ret_nodes


def services(request):
    services_ids = request.GET.getlist('ids')
    services = Service.objects.filter(id__in=services_ids)
    services_pathes=[]
    for service in services :
        path = parse_path(service.path.split('#'))
        services_pathes.append(path)
    print("(((((((")
    print(services_pathes)
    failed_path = parse_path(services[0].situation.localized_failed_path.split("#"))
    print(failed_path)
    localized_statuses = get_localized_statuses(services_pathes, failed_path)
    print(localized_statuses)
    nodes = list(set(node for service in services_pathes for node in service))
    return render(request, 'services.html', {'services': json.dumps(services_pathes), 'localized_statuses': localized_statuses,
                                             'nodes': json.dumps(nodes)})


def nodes(request):
    nodes = request.GET.getlist('nodes')
    return render(request, 'nodes.html', {'nodes': json.dumps(nodes)})


def monitor(request):
    #TODO delete this view, it is not being used anymore
    timedates = [1, 2, 3, 4, 5, 6, 7]
    monitors = [{'values': [100, 120, 170, 167, 180, 177, 160], 'node': 'POC-3', 'meaturement': 'AMP-1-6-2'},
                {'values': [60, 80, 70, 67, 80, 77, 100], 'node': 'POC-1', 'meaturement': 'AMP-1-6-1'}]
    return render(request, 'monitor.html', {'monitors': json.dumps(monitors), 'timedates': json.dumps(timedates)})


def jaccard_uniqued(s1, s2):
    intersection = s1.intersection(s2)
    union = s1.union(s2)
    if len(intersection) == 0 and len(union) == 0:
        return 0
    i = 0
    for description in intersection:
        i += 1 / Alarm.objects.filter(description=description)[0].get_uniqueness()
    u = i
    for description in union - intersection:
        u += 1 / Alarm.objects.filter(description=description)[0].get_uniqueness()
    return i / u


def similarity(request):
    situation_id = request.GET.get('situation_1', False)
    similar_situation_id = request.GET.get('situation_2', False)

    situation = Situation.objects.get(id=situation_id)
    similar_situation = Situation.objects.get(id=similar_situation_id)

    s2s = SituationToSituation.objects.filter(situation_1_id__in=[situation_id, similar_situation_id],
                                              situation_2_id__in=[situation_id, similar_situation_id])[0]

    situation_alarms = Alarm.objects.filter(situation=situation, layer=situation.failed_layer)
    similar_situation_alarms = Alarm.objects.filter(situation=similar_situation, layer=situation.failed_layer)

    s1 = set([d.description for d in situation_alarms])
    s2 = set([d.description for d in similar_situation_alarms])
    mutual_alarms = s1.intersection(s2)
    nonmutual_alarms = [s1 - s1.intersection(s2), s2 - s1.intersection(s2)]

    services_1 = Service.objects.filter(situation=situation)
    services_2 = Service.objects.filter(situation=similar_situation)
    services_pathes_1 = []
    services_pathes_2 = []
    services_1_ids = []
    services_2_ids = []
    for service in services_1:
        path = parse_path(service.path.split('#'))
        services_pathes_1.append(path)
        services_1_ids.append(service.id)

    for service in services_2:
        path = parse_path(service.path.split('#'))
        services_pathes_2.append(path)
        services_2_ids.append(service.id)

    nodes = list(set(node for service in services_pathes_1 for node in service))
    nodes2 = list(set(node for service in services_pathes_2 for node in service))
    print("@@@@@@@@@@@@@@@@@@@@@@@@")
    localized_statuses = get_localized_statuses(services_pathes_1, parse_path(situation.localized_failed_path.split("#")))
    localized_statuses2 = get_localized_statuses(services_pathes_2, parse_path(similar_situation.localized_failed_path.split("#")))
    print(localized_statuses)
    print(localized_statuses2)
    return render(request, 'similarity.html', {'situation': situation, 'similar_situation': similar_situation,
                                               's2s': s2s, 'mutual_alarms': mutual_alarms, 'situation_alarms':
                                               situation_alarms, 'similar_situation_alarms':
                                               similar_situation_alarms, 'nonmutual_alarms': nonmutual_alarms,
                                               'services': json.dumps(static_services),
                                               'localized_statuses': localized_statuses,
                                               'nodes': json.dumps(nodes),
                                               'services2': json.dumps(static_services),
                                               'localized_statuses2': localized_statuses2,
                                               'nodes2': json.dumps(nodes2),
                                               'services_1_ids': json.dumps(services_1_ids),
                                               'services_2_ids': json.dumps(services_2_ids),
                                               })


def details(request):
    situation_id = request.GET.get('situation', False)
    situation = Situation.objects.get(id=situation_id)
    alarms = Alarm.objects.filter(situation=situation, layer=situation.failed_layer).exclude(id=situation.root_alarm.id)

    alarms = sorted(alarms, key=lambda t: (t.get_performance_change() * t.get_uniqueness()))

    alarms = [situation.root_alarm] + alarms
    timedates = []
    failure_time = situation.raised
    for i in range(6):
        timedates.append((failure_time - timedelta(minutes=15*i)).replace(microsecond=0).replace(tzinfo=None))

    resources = []
    perf = []
    nodes = situation.get_affected_nodes()
#TODO: should be changed to read it online using get_performances.py during handling the failure
    if situation.failed_layer == 0:
        if 'EDTNLABPOC-02' in nodes:
            perf = {'EDTNLABPOC-01 AMP-1-2':92,
            'EDTNLABPOC-03 AMP-1-6':81,
            'EDTNLABPOC-01 AMP-1-6':62,
            'EDTNLABPOC-03 AMP-1-2':61,
            'EDTNLABPOC-02 AMP-1-2':50,
            'EDTNLABPOC-02 AMP-1-6':50}
        else:
            perf = {'EDTNLABPOC-01 AMP-1-6':91,
            'EDTNLABPOC-03 AMP-1-2':80,
            'EDTNLABPOC-01 AMP-1-2':61,
            'EDTNLABPOC-03 AMP-1-6':62}
    else:
        perf = {'FW9500-SITE1 1-6':90,
        'FW9500-SITE3 1-6':70,
        'FW9500-SITE1 1-1':30,
        'FW9500-SITE3 1-1':30}

    i=0
    performances = []
    for percent in perf.values():
        i+=1
        val = random.randint(100, 120)
        arr=[val,val,val,val,val]
        if i > 2:
            arr.append(int(val - (val*percent)/100))
        else:
            arr.append(int(val - (val*percent)/100))
        performances.append(arr)

    return render(request, 'details.html', {'alarms': alarms, 'timedates': json.dumps(timedates, indent=4, sort_keys=True, default=str),
                                            'performances': json.dumps(performances), 'lables': json.dumps(list(perf.keys()))})


def csimilarity(situation_1, situations):
    uniqueness_weight = 0
    similarity_percentages = []
    similarity_situation = []
    for situation_2 in situations:
        if situation_1 == situation_2:
            continue
        if situation_2.failed_layer == situation_1.failed_layer:
            jaccard = []
            alarms_1_1 = situation_1.get_alarms(1)
            alarms_1_2 = situation_2.get_alarms(1)

            alarms_0_1 = situation_1.get_alarms(0)
            alarms_0_2 = situation_2.get_alarms(0)

            events_1 = situation_1.get_events()
            events_2 = situation_2.get_events()

            s1 = set([(alarm['description']) for alarm in alarms_0_1.values()])
            s2 = set([(alarm['description']) for alarm in alarms_0_2.values()])

            jaccard.append(jaccard_uniqued(s1, s2))

            s1 = set([(alarm['description']) for alarm in alarms_1_1.values()])
            s2 = set([(alarm['description']) for alarm in alarms_1_2.values()])

            jaccard.append(jaccard_uniqued(s1, s2))

            s1 = set([(event['notification_type'] + event['action']) for event in events_1.values()])
            s2 = set([(event['notification_type'] + event['action']) for event in events_2.values()])

            if len(s1) == 0 and len(s2) == 0:
                jaccard.append(0)
            else:
                jaccard.append((len(s1.intersection(s2))) / (len(s1.union(s2))))

            similarity_percentage = (sum(jaccard) + uniqueness_weight*jaccard[situation_1.failed_layer]) / (3+uniqueness_weight)
            similarity_percentages.append(int(similarity_percentage*100))
            similarity_situation.append(situation_2)

        else:
            similarity_percentages.append(0)
            similarity_situation.append(situation_2)

    max_index = similarity_percentages.index(max(similarity_percentages))
    if (max(similarity_percentages))>=20:
        SituationToSituation.objects.create(situation_1=situation_1, situation_2=similarity_situation[max_index],
                                            similarity=similarity_percentages[max_index])
    # s2s = SituationToSituation.objects.filter(situation_1_id__in=[situation_1_id, situation_2_id], situation_2_id__in=[situation_1_id, situation_2_id])
    # s2s = s2s[0]
    # s2s.similarity = int(similarity_percentage*100)
    # s2s.save()
    print(similarity_percentages[max_index])
    return similarity_percentages[max_index], similarity_situation[max_index]
